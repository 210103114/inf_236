#!/bin/bash

# Function to build Docker images
build_images() {
    echo "$DOCKER_PASSWORD" | docker login -u "$DOCKER_USERNAME" --password-stdin
    docker build -t "$DOCKER_USERNAME/spring-backend:latest" ./backend
    docker build -t "$DOCKER_USERNAME/react-frontend:latest" ./react-frontend/project
    docker push "$DOCKER_USERNAME/spring-backend:latest"
    docker push "$DOCKER_USERNAME/react-frontend:latest"
}

# Function to deploy Docker containers
deploy_containers() {
    apk add --no-cache openssh-client
    eval "$(ssh-agent -s)"
    echo "$SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add - > /dev/null
    mkdir -p ~/.ssh
    chmod 700 ~/.ssh
    ssh-keyscan 139.59.209.168 >> ~/.ssh/known_hosts
    chmod 644 ~/.ssh/known_hosts

    # Pull and run backend container
    ssh "$SSH_USER@139.59.209.168" "docker pull $DOCKER_USERNAME/spring-backend:latest && \
        docker run -d --name spring-backend -p 8080:8080 $DOCKER_USERNAME/spring-backend:latest"

    # Pull and run frontend container
    ssh "$SSH_USER@139.59.209.168" "docker pull $DOCKER_USERNAME/react-frontend:latest && \
        docker run -d --name react-frontend -p 3000:3000 $DOCKER_USERNAME/react-frontend:latest"
}

# Main execution
case "$1" in
    build)
        build_images
        ;;
    deploy)
        deploy_containers
        ;;
    *)
        echo "Usage: $0 {build|deploy}"
        exit 1
        ;;
esac